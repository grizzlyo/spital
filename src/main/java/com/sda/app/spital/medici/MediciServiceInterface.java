package com.sda.app.spital.medici;

import java.util.List;

public interface MediciServiceInterface {

    void insertMedic(Medici medici);

    Medici getMedici (int id);

    List<Medici> getAllMedici();

    void deleteMedici (int id);

    void deleteAllMedici();

    void updateMedici (Medici medici);

}
