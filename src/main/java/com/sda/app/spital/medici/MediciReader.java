package com.sda.app.spital.medici;

import com.sda.app.spital.display.Display;
import com.sda.app.spital.utils.Consts;
import com.sun.java.swing.plaf.motif.MotifEditorPaneUI;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MediciReader {

    public List<Medici> readMediciFromFile(String path) {
        List<Medici> mediciList = new LinkedList<>();

        File file = new File(path);

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            String linie = reader.readLine();

            while (linie != null) {
                Medici medici = new Medici();
                String[] elements = linie.split(" ");

                medici.setId(Integer.valueOf(elements[0]));
                medici.setNume(elements[1]);
                medici.setSpecializare(elements[2]);
                medici.setVarsta(Integer.parseInt(elements[3]));

                mediciList.add(medici);

                linie = reader.readLine();

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return mediciList;
    }

    public Medici readMediciFromKeyboard(Scanner scanner) {
        Medici medici = new Medici();

        Display.displayMessage(Consts.INSERT_MEDIC_ID);
        int medicID = scanner.nextInt();
        medici.setId(medicID);

        Display.displayMessage(Consts.INSERT_MEDIC_NAME);
        String medicName = scanner.next();
        medici.setNume(medicName);

        Display.displayMessage(Consts.INSERT_MEDIC_SPECIALISATION);
        String medicSpecializare = scanner.next();
        medici.setSpecializare(medicSpecializare);

        Display.displayMessage(Consts.INSERT_MEDIC_AGE);
        int medicAge = scanner.nextInt();
        medici.setVarsta(medicAge);

        File file = new File(Consts.PATH_TO_MEDIC_FILE);

        try {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.newLine();

            bufferedWriter.write(medicID + " " + medicName + " " + medicSpecializare + " " + medicAge);
            bufferedWriter.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return medici;
    }
}
