package com.sda.app.spital.medici;

public class Medici {

    private int id;
    private String nume;
    private String specializare;
    private int varsta;

    public Medici() {
    }

    public Medici(int id, String nume, String specializare, int varsta) {
        this.id = id;
        this.nume = nume;
        this.specializare = specializare;
        this.varsta = varsta;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getSpecializare() {
        return specializare;
    }

    public void setSpecializare(String specializare) {
        this.specializare = specializare;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Medici{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", specializare='" + specializare + '\'' +
                ", varsta=" + varsta +
                '}';
    }
}
