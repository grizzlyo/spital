package com.sda.app.spital.medici;

import com.sda.app.spital.utils.Consts;

import java.util.List;

public class MediciService implements MediciServiceInterface{

    private MediciDao mediciDao;

    private MediciReader mediciReader;

    public MediciReader getMediciReader(){
        return mediciReader;
    }

    public MediciService (){
        mediciDao = MediciDao.getInstance();
        mediciReader = new MediciReader();
        medicInitialization(mediciReader.readMediciFromFile(Consts.PATH_TO_MEDIC_FILE));
    }

    public void medicInitialization (List<Medici> medicis){
        for (Medici medici : medicis){
            mediciDao.insert(medici);
        }
    }

    public void insertMedic(Medici medici){
        mediciDao.insert(medici);
    }

    @Override
    public Medici getMedici(int id) {
        return mediciDao.get(id);
    }

    @Override
    public List<Medici> getAllMedici() {
        return mediciDao.getAll();
    }

    @Override
    public void deleteMedici(int id) {
        mediciDao.delete(id);
    }

    @Override
    public void deleteAllMedici() {
        mediciDao.deleteAll();
    }

    @Override
    public void updateMedici(Medici medici) {
        mediciDao.update(medici);
    }
}
