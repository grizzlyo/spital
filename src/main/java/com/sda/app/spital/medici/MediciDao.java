package com.sda.app.spital.medici;

import com.sda.app.spital.GenericCRUD;

import java.util.LinkedList;
import java.util.List;

public class MediciDao extends GenericCRUD <Medici> {

    private List<Medici> listaMedici;
    private static MediciDao instance;

    private MediciDao (){
        listaMedici = new LinkedList<>();
    }

    public static MediciDao getInstance(){
        if (instance == null){
            instance = new MediciDao();
        }
        return instance;
    }

    @Override
    public void insert(Medici element) {
        this.listaMedici.add(element);
    }

    @Override
    public Medici get(int id) {
        for (Medici medici : listaMedici){
            if (id == medici.getId()){
                return medici;
            }
        }
        return null;
    }

    @Override
    public List<Medici> getAll() {
        return listaMedici;
    }

    @Override
    public void delete(int id) {
        for (Medici medici : listaMedici){
            if (id == medici.getId()){
                this.listaMedici.remove(medici);
                break;
            }
        }
    }

    @Override
    public void deleteAll() {
        listaMedici.clear();

    }

    @Override
    public void update(Medici element) {
        for (Medici medici : listaMedici){
            if (element.getId() == medici.getId()){
            medici.setNume(element.getNume());
            medici.setSpecializare(element.getSpecializare());
            medici.setVarsta(element.getVarsta());
            }
        }

    }
}
