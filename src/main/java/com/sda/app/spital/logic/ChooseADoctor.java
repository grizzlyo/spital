package com.sda.app.spital.logic;

import com.sda.app.spital.medici.Medici;
import com.sda.app.spital.medici.MediciDao;
import com.sda.app.spital.medici.MediciReader;
import com.sda.app.spital.medici.MediciService;
import com.sda.app.spital.utils.Consts;

import java.util.List;

public class ChooseADoctor {

    private MediciReader mediciReader;

    public ChooseADoctor() {
        mediciReader = new MediciReader();
    }

    public void alegUnMedic(){
        List<Medici>mediciList = mediciReader.readMediciFromFile(Consts.PATH_TO_MEDIC_FILE);
        int i = 1;
        for (Medici medici : mediciList){
            System.out.println(i+ Consts.SEPARATOR +medici.getSpecializare());
            i++;
        }
    }

}
