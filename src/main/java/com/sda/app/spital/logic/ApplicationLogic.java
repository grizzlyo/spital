package com.sda.app.spital.logic;

import com.sda.app.spital.display.Display;
import com.sda.app.spital.medici.Medici;
import com.sda.app.spital.medici.MediciService;
import com.sda.app.spital.pacienti.Pacienti;
import com.sda.app.spital.pacienti.PacientiService;
import com.sda.app.spital.utils.Consts;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class ApplicationLogic {

    private MediciService mediciService;
    private PacientiService pacientiService;
    private ChooseADoctor chooseADoctor;

    public ApplicationLogic() {
        mediciService = new MediciService();
        pacientiService = new PacientiService();
        chooseADoctor = new ChooseADoctor();
    }

    public void runLogic() {
        Scanner scanner = new Scanner(System.in);
        boolean isRun = true;

        Display.displayMessage(Consts.WELCOME_MESSAGE);
        Display.newLine();

        while (isRun) {
            Display.displayMessage(Consts.DISPLAY_ALL_PATIENTS);
            Display.displayMessage(Consts.DISPLAY_ALL_DOCTORS);
            Display.displayMessage(Consts.INSERT_NEW_PATIENT);
            Display.displayMessage(Consts.INSERT_NEW_DOCTOR);
            Display.displayMessage(Consts.MAKE_A_CONSULTATION);
            Display.displayMessage(Consts.DELETE_A_PATIENT);
            Display.displayMessage(Consts.EXIT);

            int option = scanner.nextInt();

            try {

                switch (option) {
                    case 1:
                        List<Pacienti> pacienti = pacientiService.getAllPacienti();
                        Display.displayAllPatients(pacienti);
                        Display.newLine();
                        break;
                    case 2:
                        List<Medici> medici = mediciService.getAllMedici();
                        Display.displayAllDoctors(medici);
                        Display.newLine();
                        break;
                    case 3:
                        Pacienti pacient = pacientiService.getPacientiReader().readPacientiFromKeyboard(scanner);
                        pacientiService.insertPacient(pacient);
                        break;
                    case 4:
                        Medici medic = mediciService.getMediciReader().readMediciFromKeyboard(scanner);
                        mediciService.insertMedic(medic);
                        break;
                    case 5:
                        Display.displayMessage(Consts.CHOOSE_A_DOCTOR);
                        Display.newLine();
                        chooseADoctor.alegUnMedic();
                        Display.newLine();
                        int doctor = scanner.nextInt();

                        if (doctor >= 1 && doctor <= mediciService.getAllMedici().size()) {

                            Display.displayMessage(Consts.DOCTOR_WELCOME_MESSAGE);
                            Display.displayMessage(Consts.CONSULTATIA_DUREAZA_DE_LA_CAZ);
                            Display.newLine();
                            Thread.sleep(10000);

                            Display.displayDiagnostic();
                            Display.newLine();
                        } else {
                            Display.displayMessage(Consts.DOCTOR_INEXISTENT);
                            Display.newLine();
                        }
                        break;

                    case 6:
                        List<Pacienti> pacienti1 = pacientiService.getAllPacienti();
                        Display.displayAllPatients(pacienti1);
                        Display.newLine();
                        Display.displayMessage(Consts.DELETE_PATIENT);
                        int ID = scanner.nextInt();
                        if (ID >= 1 && ID <= pacientiService.getAllPacienti().size()) {
                            pacientiService.deletePacienti(ID);
                        } else {
                            Display.displayMessage(Consts.PATIENT_INEXISTENT);
                            Display.newLine();
                        }
                        Display.displayAllPatients(pacienti1);
                        Display.newLine();
                        break;

                    case 7:
                        isRun = false;
                        break;

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

