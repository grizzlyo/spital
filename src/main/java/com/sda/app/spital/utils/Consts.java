package com.sda.app.spital.utils;

import java.util.ArrayList;
import java.util.List;

public interface Consts {

    /*
    mesaje cai fisiere
     */
    public static final String PATH_TO_MEDIC_FILE = "C:\\Users\\vasile\\Desktop\\Proiecte Java\\Spital\\src\\main\\resources\\medici.txt";
    public static final String PATH_TO_PACIENTI_FILE = "C:\\Users\\vasile\\Desktop\\Proiecte Java\\Spital\\src\\main\\resources\\pacienti.txt";

    /*
    mesaje introducere medici noi de la tastatura
     */
    public static final String INSERT_MEDIC_ID = "Introduceti ID-ul medicului: ";
    public static final String INSERT_MEDIC_NAME = "Introduceti numele medicului:";
    public static final String INSERT_MEDIC_SPECIALISATION = "Introduceti specializarea medicului: ";
    public static final String INSERT_MEDIC_AGE = "Introduceti varsta medicului:";

    /*
    mesaje introducere pacienti noi de la tastatura
     */
    public static final String INSERT_PATIENT_ID = "Introduceti ID-ul pacientului: ";
    public static final String INSERT_PATIENT_NAME = "Introduceti numele pacientului:";
    public static final String INSERT_PATIENT_COUNTY = "Introduceti judetul de resedinta: ";
    public static final String INSERT_PATIENT_AGE = "Introduceti varsta pacientului:";

    /*
    mesaje initiale
     */
    public static final String WELCOME_MESSAGE= "Bine ati venit la spitalul nostru!";
    public static final String DISPLAY_ALL_PATIENTS = "1. Afiseaza toti pacientii;";
    public static final String DELETE_A_PATIENT = "6. Sterge pacient;";
    public static final String DISPLAY_ALL_DOCTORS = "2. Afiseaza toti medicii;";
    public static final String INSERT_NEW_PATIENT = "3. Introdu un pacient nou;";
    public static final String INSERT_NEW_DOCTOR = "4. Introdu un medic nou;";
    public static final String MAKE_A_CONSULTATION = "5. Efectuati o consultatie;";
    public static final String EXIT = "7. Exit!";

    public static final String CHOOSE_A_DOCTOR = "Alegeti un medic la care doriti sa fiti consultat, din lista de medici disponibili momentan:";
    public static final String SEPARATOR = ". ";
    public static final String DOCTOR_WELCOME_MESSAGE = "Intrati la consultatie, pentru a primi un diagnostic;";
    public static final String CONSULTATIA_DUREAZA_DE_LA_CAZ ="Consulatatia poate dura, mai mult sau mai putin, de la caz, la caz;";
    public static final String DOCTOR_INEXISTENT = "Nu avem acest medic diponibil momentan!";
    public static final String DELETE_PATIENT = "Alegeti pacientul pe care doriti sa-l stergeti din lista, dupa ID:";
    public static final String PATIENT_INEXISTENT = "Nu avem acest pacient inregistrat!";
    /*
    mesaje diagnostice
     */
    public static final String DIAGNOSTIC_1 = "Suferiti de paraplezie locala de grad 1!";
    public static final String DIAGNOSTIC_2 = "Suferiti de cerebolita generalizata!";
    public static final String DIAGNOSTIC_3 = "Suferiti de indeferentita precoce!";
    public static final String DIAGNOSTIC_4 = "Suferiti de puturosenie maligna!";
    public static final String DIAGNOSTIC_5 = "Suferiti de dureraria nustiensis!";
    public static final String DIAGNOSTIC_6 = "Suferiti de nesimtitita severa!";
    public static final String DIAGNOSTIC_7 = "Suferiti de prostrime severa generalizata!";






}
