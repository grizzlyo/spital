package com.sda.app.spital.runner;

import com.sda.app.spital.logic.ApplicationLogic;

public class ApplicationRunner {

    public static void main(String[] args) {
        ApplicationLogic logic = new ApplicationLogic();
        logic.runLogic();
    }

}
