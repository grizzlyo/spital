package com.sda.app.spital.pacienti;

public class Pacienti {

    private int id;
    private String nume;
    private int varsta;
    private String judet;

    public Pacienti() {
    }

    public Pacienti(int id, String nume, int varsta, String judet) {
        this.id = id;
        this.nume = nume;
        this.varsta = varsta;
        this.judet = judet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public String getJudet() {
        return judet;
    }

    public void setJudet(String judet) {
        this.judet = judet;
    }

    @Override
    public String toString() {
        return "Pacienti{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", varsta=" + varsta +
                ", judet='" + judet + '\'' +
                '}';
    }
}
