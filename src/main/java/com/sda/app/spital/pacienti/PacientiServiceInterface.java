package com.sda.app.spital.pacienti;

import java.util.List;

public interface PacientiServiceInterface {

    void insertPacient(Pacienti pacienti);

    Pacienti getPacienti (int id);

    List<Pacienti> getAllPacienti();

    void deletePacienti (int id);

    void deleteAllPacienti();

    void updatePacienti (Pacienti pacienti);

}
