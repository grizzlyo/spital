package com.sda.app.spital.pacienti;

import com.sda.app.spital.display.Display;
import com.sda.app.spital.utils.Consts;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PacientiReader {

    public List<Pacienti> readPacientiFromFile(String path) {
        List<Pacienti> pacientiList = new LinkedList<>();
        File file = new File(path);

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            String linie = reader.readLine();

            while (linie != null) {
                String[] elements = linie.split(" ");
                Pacienti pacienti = new Pacienti(Integer.parseInt(elements[0]), elements[1], Integer.parseInt(elements[2]), elements[3]);
                pacientiList.add(pacienti);

                linie = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return pacientiList;
    }

    public Pacienti readPacientiFromKeyboard(Scanner scanner) {
        Pacienti pacienti = new Pacienti();

        Display.displayMessage(Consts.INSERT_PATIENT_ID);
        int patientID = scanner.nextInt();
        pacienti.setId(patientID);

        Display.displayMessage(Consts.INSERT_PATIENT_NAME);
        String patientName = scanner.next();
        pacienti.setNume(patientName);

        Display.displayMessage(Consts.INSERT_PATIENT_AGE);
        int patientAge = scanner.nextInt();
        pacienti.setVarsta(patientAge);

        Display.displayMessage(Consts.INSERT_PATIENT_COUNTY);
        String patientCounty = scanner.next();
        pacienti.setJudet(patientCounty);

        File file = new File(Consts.PATH_TO_PACIENTI_FILE);

        try {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.newLine();

            bufferedWriter.write(patientID+" "+patientName+" "+patientAge+" "+patientCounty);
            bufferedWriter.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return pacienti;
    }
}
