package com.sda.app.spital.pacienti;

import com.sda.app.spital.medici.Medici;
import com.sda.app.spital.medici.MediciServiceInterface;
import com.sda.app.spital.utils.Consts;

import java.util.List;

public class PacientiService implements PacientiServiceInterface {

    private PacientiDao pacientiDao;
    private PacientiReader pacientiReader;

    public PacientiReader getPacientiReader() {
        return pacientiReader;
    }

    public PacientiService() {
        pacientiDao = PacientiDao.getInstance();
        pacientiReader = new PacientiReader();
        patientInitialization(pacientiReader.readPacientiFromFile(Consts.PATH_TO_PACIENTI_FILE));
    }

    public void patientInitialization(List<Pacienti> pacientList) {
        for (Pacienti pacienti : pacientList) {
            pacientiDao.insert(pacienti);
        }
    }

    @Override
    public void insertPacient(Pacienti pacienti) {
        pacientiDao.insert(pacienti);
    }

    @Override
    public Pacienti getPacienti(int id) {
        return pacientiDao.get(id);
    }

    @Override
    public List<Pacienti> getAllPacienti() {
        return pacientiDao.getAll();
    }

    @Override
    public void deletePacienti(int id) {
        pacientiDao.delete(id);
    }

    @Override
    public void deleteAllPacienti() {
        pacientiDao.deleteAll();
    }

    @Override
    public void updatePacienti(Pacienti pacienti) {
        pacientiDao.update(pacienti);
    }
}
