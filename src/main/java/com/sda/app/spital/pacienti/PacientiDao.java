package com.sda.app.spital.pacienti;

import com.sda.app.spital.GenericCRUD;

import java.util.LinkedList;
import java.util.List;

public class PacientiDao extends GenericCRUD<Pacienti> {

    private List<Pacienti> patientsList;
    public static PacientiDao instance;

    public PacientiDao() {
        patientsList = new LinkedList<>();
    }

    public static PacientiDao getInstance() {
        if (instance == null) {
            instance = new PacientiDao();
        }
        return instance;
    }

    @Override
    public void insert(Pacienti element) {
        this.patientsList.add(element);
    }

    @Override
    public Pacienti get(int id) {
        for(Pacienti pacienti : patientsList){
            if(id == pacienti.getId()){
                return pacienti;
            }
        }
        return null;
    }

    @Override
    public List<Pacienti> getAll() {
        return patientsList;
    }

    @Override
    public void delete(int id) {
        for(Pacienti pacienti : patientsList){
            if(id == pacienti.getId()){
                this.patientsList.remove(pacienti);
                break;
            }
        }

    }

    @Override
    public void deleteAll() {
        patientsList.clear();
    }

    @Override
    public void update(Pacienti element) {
        for (Pacienti pacienti : patientsList){
            if (element.getId() == pacienti.getId()){
                pacienti.setId(element.getId());
                pacienti.setNume(element.getNume());
                pacienti.setVarsta(element.getVarsta());
                pacienti.setJudet(element.getJudet());
            }
        }
    }
}
