package com.sda.app.spital.display;

import com.sda.app.spital.medici.Medici;
import com.sda.app.spital.medici.MediciService;
import com.sda.app.spital.pacienti.Pacienti;
import com.sda.app.spital.utils.Consts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Display {

    public static void displayMessage(String message) {
        System.out.println(message);
    }

    public static void newLine() {
        System.out.println();
    }

    public static void displayAllDoctors(List<Medici> medicis) {
        for (Medici i : medicis) {
            System.out.println(i);

        }
    }

    public static void displayAllPatients(List<Pacienti> pacienti) {
        for (Pacienti i : pacienti) {
            System.out.println(i);

        }
    }

    public static void displayDiagnostic (){
        List<String> diagnostice = new ArrayList<>();
        diagnostice.add(Consts.DIAGNOSTIC_1);
        diagnostice.add(Consts.DIAGNOSTIC_2);
        diagnostice.add(Consts.DIAGNOSTIC_3);
        diagnostice.add(Consts.DIAGNOSTIC_4);
        diagnostice.add(Consts.DIAGNOSTIC_5);
        diagnostice.add(Consts.DIAGNOSTIC_6);
        diagnostice.add(Consts.DIAGNOSTIC_7);

        Random r = new Random();

        System.out.println(diagnostice.get(r.nextInt(diagnostice.size())));

    }



}
